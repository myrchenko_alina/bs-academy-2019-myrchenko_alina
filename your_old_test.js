const assert = require('assert');
const {URL} = require('url');

describe('webdriver.io page', () => {
    xit('should have the right title', () => {
        browser.url('https://webdriver.io');
        const title = browser.getTitle();
        assert.equal(title, 'WebdriverIO · Next-gen WebDriver test framework for Node.js');
    });

    xit('should be registered on Hedonist', () => {
        const siteUrl = 'https://staging.bsa-hedonist.online';
        browser.url(siteUrl);

        const registerBtn = $('a.link.link-signup');
        registerBtn.click();

        browser.waitUntil(() => {const url = new URL(browser.getUrl());
            const actualUrl = url.host.toString() + url.pathname.toString();
            return (actualUrl === 'staging.bsa-hedonist.online/signup');});

        const firstNameField = $('input[name=firstName]');
        firstNameField.setValue('John');

        const lastNameField = $('input[name=lastName]');
        lastNameField.setValue('Johnson');

        const emailField = $('input[name=email]');
        emailField.setValue('testmail2@gmail.com');

        const passField = $('input[type=password]');
        passField.setValue('qwerty123');

        const confirmRegisterBtn = $('button.button.is-primary');
        confirmRegisterBtn.click();

        logIn(siteUrl, 'testmail2@gmail.com', 'qwerty123');

        browser.waitUntil(() => {const url = new URL(browser.getUrl());
            const actualUrl = url.host.toString() + url.pathname.toString();
            return (actualUrl === 'staging.bsa-hedonist.online/search');});

        const url = new URL(browser.getUrl());
        const actualUrl = url.host.toString() + url.pathname.toString();

        assert.equal(actualUrl,"staging.bsa-hedonist.online/search");
    });

    xit('should be unable to log in with invalid credentials', () => {
        const siteUrl = 'https://staging.bsa-hedonist.online';

        logIn(siteUrl, 'test@te-st.com', 'asdkl;');

        const alertPopUp = $('div.notices.is-top');
        alertPopUp.waitForDisplayed();

        const url = new URL(browser.getUrl());
        const actualUrl = url.host.toString() + url.pathname.toString();

        assert.equal(actualUrl,"staging.bsa-hedonist.online/login");
    });

    it('should be able to add a new place', () => {
        let siteUrl = 'https://staging.bsa-hedonist.online';

        logIn(siteUrl, 'testmail1@gmail.com', 'qwerty123');
       
        browser.waitUntil(() => {const url = new URL(browser.getUrl());
            const actualUrl = url.host.toString() + url.pathname.toString();
            return (actualUrl === 'staging.bsa-hedonist.online/search');});

        const dropDownMenu = $('div.navbar-dropdown-menu');
        dropDownMenu.waitForDisplayed(5000);
        dropDownMenu.waitForEnabled(5000);
        dropDownMenu.click();

        const newPlaceBtn = $('a.navbar-item[href="/places/add"]');
        newPlaceBtn.waitForExist(5000);
        newPlaceBtn.waitForEnabled(5000);
        newPlaceBtn.click();

        browser.waitUntil(() => {const url = new URL(browser.getUrl());
            const actualUrl = url.host.toString() + url.pathname.toString();
            return (actualUrl === 'staging.bsa-hedonist.online/places/add');});
        
        const placeNameInp = $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[1]/div[2]/div/div/input');
        placeNameInp.setValue('Test place');

        const placeZIPInp = $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[4]/div[2]/div/div/input');
        placeZIPInp.setValue('00000');

        const placeAddrInp = $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[5]/div[2]/div/div/input');
        placeAddrInp.setValue('Khreschatyk St., 14');

        const placePhoneInp = $('input[type=tel]');
        placePhoneInp.setValue('+380500000000');

        const placeWebsiteInp = $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[9]/div[2]/div/div/input');
        placeWebsiteInp.setValue('http://test.com');

        const placeDescrInp = $('textarea.textarea');
        placeDescrInp.setValue('We are the best place you could visit');

        let nextPageBtn = $('//*[@id="app"]/div[2]/div/div/section/div[1]/div[12]/span');
        nextPageBtn.click();

        const uploadBtn = $('input[type=file]');
        uploadBtn.setValue('C:\\Users\\user\\Documents\\GitHub\\bs-academy-2019-myrchenko_alina\\assets\\test.jpeg');

        nextPageBtn = $('//*[@id="app"]/div[2]/div/div/section/div[2]/div[2]/span[2]');
        nextPageBtn.click();

        nextPageBtn = $('//*[@id="app"]/div[2]/div/div/section/div[3]/div[2]/span[2]');
        nextPageBtn.click();

        const selectCatDropDown = $('//*[@id="app"]/div[2]/div/div/section/div[4]/div[1]/div[1]/div/div/div/span');
        selectCatDropDown.click();
        const catOption = $('//*[@id="app"]/div[2]/div/div/section/div[4]/div[1]/div[1]/div/div/div/span/select/option[2]');
        catOption.click();
        const selectTagsDropDown = $('//*[@id="app"]/div[2]/div/div/section/div[4]/div[1]/div[2]/div/div/div/span');
        selectTagsDropDown.click();
        const tagOption = $('//*[@id="app"]/div[2]/div/div/section/div[4]/div[1]/div[2]/div/div/div/span/select/option[2]');
        tagOption.click();
        nextPageBtn = $('//*[@id="app"]/div[2]/div/div/section/div[4]/div[2]/span[2]');
        nextPageBtn.click();

        const featureCheckBox = $('//*[@id="app"]/div[2]/div/div/section/div[5]/div[1]/div/div[2]/div[2]/label/input');
        featureCheckBox.checked = true;
        const featureSwitch = $('//*[@id="app"]/div[2]/div/div/section/div[5]/div[1]/div/div[1]/div[2]/label/span[1]');
        featureSwitch.click();
        nextPageBtn = $('//*[@id="app"]/div[2]/div/div/section/div[5]/div[2]/span[2]');
        nextPageBtn.waitForEnabled(3000);
        nextPageBtn.waitForDisplayed(3000);
        nextPageBtn.click();
        nextPageBtn = $('//*[@id="app"]/div[2]/div/div/section/div[6]/div[2]/span[2]');
        nextPageBtn.waitForEnabled(3000);
        nextPageBtn.waitForDisplayed(3000);
        nextPageBtn.click();
        browser.pause(3000);
        const confirmBtn = $('//*[@id="app"]/div[2]/div/div/section/div[7]/div/div[2]/span[2]');
        confirmBtn.waitForEnabled(3000);
        confirmBtn.waitForDisplayed(3000);
        confirmBtn.click();

        browser.waitUntil(() => {const url = new URL(browser.getUrl());
            const actualUrl = url.host.toString() + url.pathname.toString();
            return (actualUrl.includes('staging.bsa-hedonist.online/places'));});

        const url = new URL(browser.getUrl());
        const actualUrl = url.host.toString() + url.pathname.toString();

        assert(actualUrl.includes('/places'));
        assert.equal($('div.place-venue__place-name').getText(), "Test place");

    });

    xit('should be able to create a list', () => {
        const siteUrl = 'https://staging.bsa-hedonist.online';

        logIn(siteUrl, 'testmail1@gmail.com', 'qwerty123');
       
        browser.waitUntil(() => {const url = new URL(browser.getUrl());
            const actualUrl = url.host.toString() + url.pathname.toString();
            return (actualUrl === 'staging.bsa-hedonist.online/search');});

        const listName = 'My-list';
        const imgPath = 'C:\\Users\\user\\Documents\\GitHub\\bs-academy-2019-myrchenko_alina\\assets\\test.jpeg';

        createList(listName, imgPath);

        //should be crated and displayed on top of the list
        const listTitle = $('div.media-content > h3 > a');
        const title = listTitle.getText();

        assert.equal(title, "My-list");
    });

    xit('should be able to delete a list', () => {
        const siteUrl = 'https://staging.bsa-hedonist.online';

        logIn(siteUrl, 'testmail1@gmail.com', 'qwerty123');
       
        browser.waitUntil(() => {const url = new URL(browser.getUrl());
            const actualUrl = url.host.toString() + url.pathname.toString();
            return (actualUrl === 'staging.bsa-hedonist.online/search');});

        const listName = 'My-list';
        const imgPath = 'C:\\Users\\user\\Documents\\GitHub\\bs-academy-2019-myrchenko_alina\\assets\\test.jpeg';

        createList(listName, imgPath);

        const delListBtn = $('div.place-item__actions > button.button.is-danger');
        delListBtn.click();

        const confirmDelPopUp = $('div.animation-content.modal-content');
        confirmDelPopUp.waitForDisplayed();

        const confirmDelBtn = $('div.animation-content.modal-content button.button.is-danger');
        confirmDelBtn.click();

        const noListMsg = $('div.no-lists-text');
        noListMsg.waitForDisplayed();

        assert.equal(noListMsg.getText(), "You have not got any place lists yet. You may add some and grouped your chosen places.");
    });
});

function logIn(url, email, pswrd){
    browser.url(url);

    const emailFieldLogIn = $('input[name=email]');
    emailFieldLogIn.setValue(email);

    const passFieldLogIn = $('input[type=password]');
    passFieldLogIn.setValue(pswrd);

    const logInBtn = $('button.button.is-primary');
    logInBtn.click();
}

function createList(listName, imgPath){
    const dropDownMenu = $('div.navbar-dropdown-menu');
    dropDownMenu.waitForExist(5000);
    dropDownMenu.waitForEnabled(5000);
    dropDownMenu.click();

    const myListsBtn = $('a.navbar-item[href="/my-lists"]');
    myListsBtn.waitForExist(5000);
    myListsBtn.waitForEnabled(5000);
    myListsBtn.click();

    browser.waitUntil(() => {let url = new URL(browser.getUrl());
        let actualUrl = url.host.toString() + url.pathname.toString();
        return (actualUrl === 'staging.bsa-hedonist.online/my-lists');});
    
    const newListBtn = $('a.button[href="/my-lists/add"]');
    newListBtn.waitForExist(3000);
    newListBtn.waitForEnabled(3000);
    newListBtn.click();

    const listNameInp = $('input#list-name');
    listNameInp.waitForExist();
    listNameInp.setValue(listName);

    const imgLoad = $('input[name=photo]');
    imgLoad.setValue(imgPath);

    const saveListBtn = $('div.form-actions > button.button.is-success');
    saveListBtn.click();
}