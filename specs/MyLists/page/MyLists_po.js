class MyListsPage{

    get addNewListButton() {return $('a.button[href="/my-lists/add"]')};
    get listTitles() {return $$('//div/h3/a[contains(@href, "/list")]')};
    get deleteListButtons() {return $$('//button[contains(., "Delete")]')};
    get deleteConfirmButton() {return $('//button[contains(., "Delete")]')};
    get noListsMessage() {return $('div.no-lists-text')};
};

module.exports = MyListsPage;