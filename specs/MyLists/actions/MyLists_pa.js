const MyListsPage = require('../page/MyLists_po');
const page = new MyListsPage();

class MyListsActions{

    addNewList(){
        page.addNewListButton.waitForDisplayed(2000);
        page.addNewListButton.click();
        /*var selector = page.addNewListButton;
        browser.execute((el) => {
            document.querySelectorAll(el)[0].click();
        }, selector);*/
    };

    getListTitleLink(index){
        page.listTitles[index].waitForDisplayed(2000);
        return page.listTitles[index];
    };

    getListTitleText(index){
        page.listTitles[index].waitForDisplayed(2000);
        return page.listTitles[index].getText();
    };

    confirmDelete(){
        page.deleteConfirmButton.waitForDisplayed(2000);
        page.deleteConfirmButton.click();
    };

    clickDeleteList(index){
        page.deleteListButtons[index].waitForDisplayed(2000);
        page.deleteListButtons[index].click();
    };

    getNoListsMessage(){
        page.noListsMessage.waitForDisplayed(2000);
        return page.noListsMessage.getText();
    }
};

module.exports = MyListsActions;