const NewListActions = require('../NewList/actions/NewList_pa');
const MyListsActions = require('../MyLists/actions/MyLists_pa');
const MenuActions = require('../Menu/actions/menu_pa');
const Wait = require('../../helpers/waiters');
const Page = require('../../helpers/helpers');
const assert = require('assert');
const credentials = require('./../testData.json');

const menuSteps = new MenuActions();
const newListSteps = new NewListActions();
const pageSteps = new MyListsActions();

describe('My lists page test', () =>{
    beforeEach(() =>{
        Page.loginWithDefaultUser();
        Wait.forNotificationToDisappear();
        Wait.forSpinner();
        menuSteps.navigateToLists();
        Wait.forSpinner();
    });

    afterEach(() =>{
        browser.reloadSession();
    });

    it('should be able to delete a list', () =>{
        Page.createPlentyLists();

        const initialLink = pageSteps.getListTitleLink(0);
        pageSteps.clickDeleteList(0);
        Wait.forModal();
        pageSteps.confirmDelete();
        Wait.forSpinner();
        assert.notEqual(pageSteps.getListTitleLink(0), initialLink);
    });
});