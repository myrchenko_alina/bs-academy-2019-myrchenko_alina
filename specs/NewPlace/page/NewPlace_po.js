class NewPlacePage{

    get placeNameInput() {return $('//div/label[contains(., "Name")]/../..//input')};
    get placeCityInput() {return $('//div/label[contains(., "City")]/../..//input')};
    get placeZipInput() {return $('//div/label[contains(.,"Zip")]/../..//input')};
    get placeAddressInput() {return $('//div/label[contains(.,"Address")]/../..//input')};
    get placePhoneInput() {return $('//div/label[contains(.,"Phone")]/../..//input[@type="tel"]')};
    get placeWebsiteInput() {return $('//div/label[contains(.,"Website")]/../..//input')};
    get placeDescriptionInput() {return $('//div/label[contains(.,"Description")]/../..//textarea')};
    get nextPageButtons() {return $$('//div/span[contains(., "Next")]')};
    get uploadPictureButton() {return $('//input[@type="file"]')};
    get selectCategoryDropdown() {return $('//div/span[@class="select"]//option[contains(., "Select a category")]')};
    get dropdownCategoryOption() {return $('//div/span[@class="select"]//option[contains(., "Select a category")]/../option[2]')};
    get selectTagsDropdown() {return $('//div/span[@class="select"]//option[contains(., "Add tags")]')};
    get dropdownTagOption() {return $('//div/span[@class="select"]//option[contains(., "Add tags")]/../option[2]')};
    get featureCheckbox() {return $('//div/label[@class="switch"]/input')};
    get featureSwitch() {return $('//div/label[@class="switch"]/span[1]')};
    get addPlaceButton() {return $('//div/span[contains(., "Add")][@class="button is-success"]')};

};

module.exports = NewPlacePage;
