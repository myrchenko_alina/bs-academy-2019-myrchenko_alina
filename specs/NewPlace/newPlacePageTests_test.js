const NewPlaceActions = require('../NewPlace/actions/NewPlace_pa');
const MenuActions = require('../Menu/actions/menu_pa');
const PlaceActions = require('../Place/actions/Place_pa');
const Page = require('../../helpers/helpers');
const Assert = require('../../helpers/validators');
const Wait = require('../../helpers/waiters');
const assert = require('assert');
const credentials = require('./../testData.json');

const menuSteps = new MenuActions();
const pageSteps = new NewPlaceActions();
const placeSteps = new PlaceActions();

describe('Adding a new place page tests', () =>{

    beforeEach(() =>{
        Page.loginWithDefaultUser();
        Wait.forSpinner();
        menuSteps.navigateToNewPlace();
    });

    afterEach(() =>{
        browser.reloadSession();
    });

    it('should be able to add a new place', () =>{
        pageSteps.enterPlaceName(credentials.placeName);
        pageSteps.enterPlaceZip(credentials.placeZip);
        pageSteps.enterPlaceAddress(credentials.placeAddress);
        pageSteps.enterPlacePhone(credentials.placePhone);
        pageSteps.enterPlaceWebsite(credentials.placeWebsite);
        pageSteps.enterPlaceDescription(credentials.placeDescription);
        pageSteps.navigateToNextPage(0);
        pageSteps.uploadPicture(credentials.picturePath);
        pageSteps.navigateToNextPage(1);
        pageSteps.navigateToNextPage(2);
        pageSteps.selectPlaceCategory();
        pageSteps.selectPlaceTags();
        pageSteps.navigateToNextPage(3);
        pageSteps.selectPlaceFeatures();
        pageSteps.navigateToNextPage(4);
        pageSteps.navigateToNextPage(5);
        pageSteps.addNewPlace();
        Wait.forSpinner();

        Assert.redirectedTo("/places");
        assert.equal(placeSteps.getPlaceName(), credentials.placeName);
    });
});