const NewPlacePage = require('../page/NewPlace_po');
const page = new NewPlacePage();

class NewPlaceActions{

    enterPlaceName(value){
        page.placeNameInput.waitForDisplayed(5000);
        page.placeNameInput.clearValue();
        page.placeNameInput.setValue(value);
    };

    enterPlaceCity(value){
        page.placeCityInput.waitForDisplayed(2000);
        page.placeCityInput.clearValue();
        page.placeCityInput.setValue(value);
    };

    enterPlaceZip(value){
        page.placeZipInput.waitForDisplayed(2000);
        page.placeZipInput.clearValue();
        page.placeZipInput.setValue(value);
    };

    enterPlaceAddress(value){
        page.placeAddressInput.waitForDisplayed(2000);
        page.placeAddressInput.clearValue();
        page.placeAddressInput.setValue(value);
    };

    enterPlacePhone(value){
        page.placePhoneInput.waitForDisplayed(2000);
        page.placePhoneInput.clearValue();
        page.placePhoneInput.setValue(value);
    };

    enterPlaceWebsite(value){
        page.placeWebsiteInput.waitForDisplayed(2000);
        page.placeWebsiteInput.clearValue();
        page.placeWebsiteInput.setValue(value);
    };

    enterPlaceDescription(value){
        page.placeDescriptionInput.waitForDisplayed(2000);
        page.placeDescriptionInput.clearValue();
        page.placeDescriptionInput.setValue(value);
    };

    navigateToNextPage(index){
        page.nextPageButtons[index].waitForDisplayed(5000);
        page.nextPageButtons[index].waitForEnabled(5000);
        page.nextPageButtons[index].click();
        browser.pause(1000);
    };

    uploadPicture(path){
        page.uploadPictureButton.waitForEnabled(2000);
        page.uploadPictureButton.setValue(path);
    };

    _getCategoriesOptions(){
        page.selectCategoryDropdown.waitForDisplayed(2000);
        page.selectCategoryDropdown.click();
    };

    selectPlaceCategory(){
        this._getCategoriesOptions();
        page.dropdownCategoryOption.waitForDisplayed(2000);
        page.dropdownCategoryOption.click();
    };

    _getTagsOptions(){
        page.selectTagsDropdown.waitForDisplayed(2000);
        page.selectTagsDropdown.click();
    };

    selectPlaceTags(){
        this._getTagsOptions();
        page.dropdownTagOption.waitForDisplayed(2000);
        page.dropdownTagOption.click();
    };

    selectPlaceFeatures(){
        page.featureCheckbox.waitForEnabled(2000);
        page.featureCheckbox.checked = true;
        page.featureSwitch.waitForEnabled(2000);
        page.featureSwitch.click();
    };

    addNewPlace(){
        page.addPlaceButton.waitForDisplayed(3000);
        page.addPlaceButton.waitForEnabled(3000);
        page.addPlaceButton.click();
    };
};

module.exports = NewPlaceActions;
