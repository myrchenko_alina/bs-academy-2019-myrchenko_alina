class ListPage{

    get listTitle() {return $('//h3[@class="title"]')};
    get listCreator() {return $('//a[contains(@href, "users")][@class = "has-text-info"]')};
};

module.exports = ListPage;