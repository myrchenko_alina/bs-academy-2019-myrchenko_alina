class SearchPage{

    get placeSearchField() {return $('//input[@placeholder="I\'m looking for..."]')};
    get placeSearchFieldDropdownItems() {return $$('//input[@placeholder="I\'m looking for..."]/../following-sibling::*//a[@class="dropdown-item"]')}; 
    get citySearchField() {return $('//input[@placeholder="Location"]')};
    get citySearchFieldDropdownItems() {return $$('//*[@id="app"]/div/nav/div/div[3]/div[1]/div[2]/div/div/div/div[2]/div/a')};
    get searchButton() {return $('//button//span[contains(., "Search")]/..')};
    get searchResultsSection() {return $('//*[@id="app"]/section/section[1]/div[1]')};
    get searchResultItems() {return $$('//div[contains(@class, "place-item")]')};
    get placeCounterNumber() {return $('//span[contains(@class, "place-counter__number")]')};
    get lastResultItem() {return $('//div[contains(@class, "place-item")][last()]')};
};

module.exports = SearchPage;