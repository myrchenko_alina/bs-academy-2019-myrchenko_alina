const SearchActions = require('../Search/actions/Search_pa');
const Page = require('../../helpers/helpers');
const Assert = require('../../helpers/validators');
const Wait = require('../../helpers/waiters');
const credentials = require('./../testData.json');

const pageSteps = new SearchActions();

describe('Search page tests', () =>{

    beforeEach(() =>{
        Page.loginWithDefaultUser();
        Wait.forSpinner();
    });

    afterEach(() =>{
        browser.reloadSession();
    });

    it('should display correct amount of search results', () =>{
        pageSteps.enterSearchPlace();
        pageSteps.enterSearchCity("Kiev");
        pageSteps.startSearching();
        Wait.forSpinner();

        try{
            pageSteps.moveToSearchResultsSection();
            Page.scrollSectionToBottom(credentials.searchResListClass, 15);
        }
        catch(err){
            console.error(err);
        }
        const resultItems = pageSteps.getSearchResultItems();
        const counterValue = pageSteps.getPlaceCounterNumber();
        Assert.elementCountIs(resultItems, Number(counterValue));
    });
});