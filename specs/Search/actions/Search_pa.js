const SearchPage = require('../page/Search_po');
const page = new SearchPage();

class SearchActions{

    _choosePlaceDropDownOption(){
        page.placeSearchFieldDropdownItems[2].waitForDisplayed(2000);
        page.placeSearchFieldDropdownItems[2].click();
    };

    enterSearchPlace(){
        page.placeSearchField.waitForDisplayed(2000);
        page.placeSearchField.click();
        this._choosePlaceDropDownOption();
    };

    _chooseCityDropDownOption(){
        browser.waitUntil(() => {return page.citySearchFieldDropdownItems[0].isExisting();})
        //page.citySearchFieldDropdownItems[0].waitForEnabled(5000);
        page.citySearchFieldDropdownItems[0].click();
    };

    enterSearchCity(value){
        page.citySearchField.waitForDisplayed(2000);
        page.citySearchField.click();
        page.citySearchField.clearValue();
        page.citySearchField.setValue(value);
        page.citySearchField.click();
        //browser.pause(10000);
        this._chooseCityDropDownOption();
    };

    startSearching(){
        page.searchButton.waitForDisplayed(2000);
        page.searchButton.click();
    };

    moveToSearchResultsSection(){
        page.searchResultsSection.waitForDisplayed(10000);
        page.searchResultsSection.moveTo();
    };

    scrollSearchSectionToBottom(){
        //this._moveToSearchResultsSection();
        if (page.searchResultItems.length === 0) {
            throw new Error("Element not found");
        }
        for(let i = 14; page.searchResultItems.length >= i; i+=15){
            page.searchResultItems[i].scrollIntoView();
            try{
                browser.waitUntil(() => {
                    return (page.searchResultItems[i+15].isExisting() || (page.searchResultItems.length <= (i+15))) ;
                }, 5000);
            }
            catch(err){
                console.error(err);
            }
        }
    };

    getSearchResultItems(){
        page.searchResultItems[0].waitForExist(2000);
        page.searchResultItems[0].waitForDisplayed(2000);
        return page.searchResultItems;
    }

    countResultsAmount(){
        return page.searchResultItems.length;
    };

    getPlaceCounterNumber(){
        return page.placeCounterNumber.getText();
    };

};

module.exports = SearchActions;