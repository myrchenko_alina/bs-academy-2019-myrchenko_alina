class LoginPage{

    get emailInput() {return $('input[name=email]')};
    get passwordInput() {return $('input[type=password]')};
    get loginButton() {return $('button.button.is-primary')};
    get createNewButton() {return $('a.link.link-signup')};
    get invalidCredsAlert() {return $('div.notices.is-top')};
    
};

module.exports = LoginPage;
