const LoginPage = require('../page/Login_po');
const page = new LoginPage();

class LoginActions{

    enterEmail(value){
        page.emailInput.waitForDisplayed(2000);
        page.emailInput.clearValue();
        page.emailInput.setValue(value);
    };

    enterPassword(value){
        page.passwordInput.waitForDisplayed(2000);
        page.passwordInput.clearValue();
        page.passwordInput.setValue(value);
    };

    LogIn(){
        page.loginButton.waitForDisplayed(2000);
        page.loginButton.click();
    }

    register(){
        page.createNewButton.waitForDisplayed(2000);
        page.createNewButton.click();
    }

    waitForInvalidCredsAlert(){
        page.invalidCredsAlert.waitForDisplayed(2000);
    }
}

module.exports = LoginActions;
