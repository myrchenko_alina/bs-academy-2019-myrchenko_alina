const LoginActions = require('./actions/Login_pa');
const Assert = require('../../helpers/validators');
const Wait = require('../../helpers/waiters');
const credentials = require('./../testData.json');

const pageSteps = new LoginActions();

describe('Login page tests', () =>{

    beforeEach(() =>{
        browser.maximizeWindow();
        browser.url(credentials.appUrl);
    });

    afterEach(() =>{
        browser.reloadSession();
    });

    it('should be unable to log in with invalid credentials', () =>{
        pageSteps.enterEmail(credentials.invalidEmail);
        pageSteps.enterPassword(credentials.invalidPassword);
        pageSteps.LogIn();
        Wait.forNotificationToDisappear();

        Assert.redirectedTo(credentials.loginRoute);
    });
});