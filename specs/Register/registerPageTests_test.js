const RegisterActions = require('./actions/Register_pa');
const LoginActions = require('../Login/actions/Login_pa');
const Page = require('../../helpers/helpers');
const Wait = require('../../helpers/waiters');
const Assert = require('../../helpers/validators');
const assert = require('assert');
const credentials = require('./../testData.json');

const loginSteps = new LoginActions();
const pageSteps = new RegisterActions();

describe('Register page tests', () =>{

    beforeEach(() =>{
        browser.maximizeWindow();
        browser.url(credentials.appUrl);
        loginSteps.register();
    });

    afterEach(() =>{
        browser.reloadSession();
    });

    it('should be registered on Hedonist', () =>{
        const randomEmail = Page.getRandomEmail();
        pageSteps.enterFirstName(credentials.name);
        pageSteps.enterLastName(credentials.surname);
        pageSteps.enterEmail(randomEmail);
        pageSteps.enterPassword(credentials.password);
        pageSteps.createAccount();
        browser.pause(2000);
        
        Page.loginWithCustomUser(randomEmail, credentials.password);
        Wait.forSpinner();

        Assert.redirectedTo("/search");
    });
});