class NewListPage{

    get listNameInput() {return $('input#list-name')};
    get uploadPictureButton() {return $('input[name=photo]')};
    get saveListButton() {return $('//button[contains(., "Save")]')};
};

module.exports = NewListPage;