const NewListPage = require('../page/NewList_po');
const page = new NewListPage();

class NewListActions{

    enterListName(value){
        page.listNameInput.waitForDisplayed(5000);
        page.listNameInput.clearValue();
        page.listNameInput.setValue(value);
    };

    uploadListPicture(path){
        page.uploadPictureButton.waitForDisplayed(2000);
        page.uploadPictureButton.setValue(path);
    };

    saveList(){
        page.saveListButton.waitForDisplayed(2000);
        page.saveListButton.click();
    };
};

module.exports = NewListActions;