const NewListActions = require('../NewList/actions/NewList_pa');
const MyListsActions = require('../MyLists/actions/MyLists_pa');
const ListActons = require('../List/actions/List_pa');
const MenuActions = require('../Menu/actions/menu_pa');
const LoginActions = require('../Login/actions/Login_pa');
const Page = require('../../helpers/helpers');
const Assert = require('../../helpers/validators');
const Wait = require('../../helpers/waiters');
const assert = require('assert');
const credentials = require('./../testData.json');

const menuSteps = new MenuActions();
const listSteps = new ListActons();
const listsSteps = new MyListsActions();
const pageSteps = new NewListActions();
const loginSteps = new LoginActions();

function userLogin(email, password){
    loginSteps.enterEmail(email);
    loginSteps.enterPassword(password);
    loginSteps.LogIn();
};

describe('Adding a new list page tests', () =>{
    beforeEach(() =>{
        Page.loginWithDefaultUser();
        Wait.forSpinner();
        menuSteps.navigateToLists();
        Wait.forSpinner();
        listsSteps.addNewList();
    });

    afterEach(() =>{
        browser.reloadSession();
    });

    xit('should be able to add a new list', () =>{
        pageSteps.enterListName(credentials.listName);
        pageSteps.uploadListPicture(credentials.picturePath);
        pageSteps.saveList();
        Wait.forSpinner();

        try{
            Page.clickItemInList(credentials.listName);
        }
        catch(error){
            console.error(error);
        }

        const listTitle = listSteps.getListTitle();
        const listCreator = listSteps.getListCreator();

        assert.equal(listTitle, credentials.listName);
        assert.equal(listCreator, credentials.name + credentials.surname);

        listsSteps.clickDeleteList(0);
        Wait.forModal();
        listsSteps.confirmDelete();
        Wait.forSpinner();
    });
});