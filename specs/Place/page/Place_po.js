class PlacePage{

    get placeName() {return $('div.place-venue__place-name')};
    get reviews() {return $$('//div[@class = "review"]')};
    get reviewsCounter() {return $('//span[contains(.,"Reviews")]')};
};

module.exports = PlacePage;