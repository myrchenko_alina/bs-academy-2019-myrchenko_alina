const PlacePage = require('../page/Place_po');
const page = new PlacePage();

class PlaceActions{

    getPlaceName(){
        page.placeName.waitForDisplayed(2000);
        return page.placeName.getText();
    }

    getReviews(){
        browser.waitUntil(() => {return page.reviews[0].isExisting();})
        return page.reviews;
    }

    getReviewsCounterNum(){
        let counterNum = page.reviewsCounter.getText().replace(/[A-Za-z\(\)]/g, '');
        console.log(counterNum.trim());
        return Number(counterNum.trim());
    }
};

module.exports = PlaceActions;