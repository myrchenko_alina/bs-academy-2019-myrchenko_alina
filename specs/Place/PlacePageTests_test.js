const Page = require('../../helpers/helpers');
const Assert = require('../../helpers/validators');
const Wait = require('../../helpers/waiters');
const credentials = require('./../testData.json');
const PlaceActions = require('../Place/actions/Place_pa');

const pageSteps = new PlaceActions();

describe('Place page tests', () =>{

    beforeEach(() =>{
        Page.loginWithDefaultUser();
        Wait.forSpinner();
        browser.url(credentials.placeUrl);
    });

    afterEach(() =>{
        browser.reloadSession();
    });

    it('should display correct amount of reviews', () =>{
        Wait.forSpinner();

        try{
            Page.scrollSectionToBottom(credentials.reviewsListClass, 10);
        }
        catch(err){
            console.error(err);
        }
        const resultItems = pageSteps.getReviews();
        const counterValue = pageSteps.getReviewsCounterNum();
        Assert.elementCountIs(resultItems, Number(counterValue));
    });
});