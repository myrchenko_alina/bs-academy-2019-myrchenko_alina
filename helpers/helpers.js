const LoginActions = require('../specs/Login/actions/Login_pa');
const NewListActions = require('../specs/NewList/actions/NewList_pa');
const MyListsActions = require('../specs/MyLists/actions/MyLists_pa');
const Wait = require('../helpers/waiters');
const page = new LoginActions();
const newList = new NewListActions();
const myLists = new MyListsActions();
const credentials = require('../specs/testData.json');

class HelpClass 
{

    clickItemInList(name) {
        const place = $$(`//div[contains(@class, "place-item")]//h3/a[contains(., "${name}")]`);
        if (place.length === 0) {
            throw new Error("Element not found");
        }
        place[0].scrollIntoView();
        this.browserClick(place[0]);
    }

    browserClick(elm){
        return browser.execute((e) => {
            document.querySelectorAll(e).click();
        }, elm);
    }

    browserClickOnArrayElement(elm, index){
        return browser.execute((e, i) => {
            document.querySelectorAll(e)[i - 1].click();
        }, elm, index);
    }

    loginWithDefaultUser() {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

        page.enterEmail(credentials.email);
        page.enterPassword(credentials.password);
        page.LogIn();
    }
    
    loginWithCustomUser(email, password) {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

        page.enterEmail(email);
        page.enterPassword(password);
        page.LogIn();
    }

    selectItemFromList() {
       
    }

    getRandomEmail(){
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < 6; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        result += "@mail.com"
        return result;
    }

    getActualUrl(){
        const url = new URL(browser.getUrl());
        const actualUrl = url.host.toString() + url.pathname.toString();
        return actualUrl;
    }

    createList(){
        myLists.addNewList();
        newList.enterListName(credentials.listName);
        newList.uploadListPicture(credentials.picturePath);
        newList.saveList();
        Wait.forSpinner();
    };
    
    createPlentyLists(){
        for(let i = 0; i < 30; i++){
            this.createList();
        }
    };

    _getItemList(listClass){
        return $$(`//div[contains(@class, ${listClass})]`);
    };

    scrollSectionToBottom(listClass, step){
        if (this._getItemList(listClass).length === 0) {
            throw new Error("Element not found");
        }

        for(let i = step - 1; this._getItemList(listClass).length >= i; i+=step){
            this._getItemList(listClass)[i].scrollIntoView();
            try{
                browser.waitUntil(() => {
                    return (this._getItemList(listClass)[i+step].isExisting() || (this._getItemList(listClass).length <= (i+step))) ;
                }, 5000);
            }
            catch(err){
                console.error(err);
            }
        }
    };

}

module.exports = new HelpClass();