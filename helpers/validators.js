const assert = require('chai').assert;
const expect = require('chai').expect;

class AssertHelper {

    elementCountIs(locator, expectedQty) {
        const els = locator;
        const actualQty = els.length;

        assert.strictEqual(actualQty, expectedQty, `Expected ${expectedQty} is not equal to ${actualQty}`);
    }

    wrongValueIndicationOnField(locator) {
        const attr = locator.getAttribute('class');
        expect(attr, `${attr} doesn't include validation class`).to.include("is-danger");
    }

    wrongValueIndicationOnLable(locator) {

        const attr = locator.getAttribute('class');
        expect(attr, `${attr} doesn't include error class`).to.include("error");
    }

    errorNotificationTextIs(expectedText) {
        const notification = $('div.toast.is-danger div');
        const actualText = notification.getText()
        assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    }

    successNotificationTextIs(expectedText) {
        const notification = $('div.toast.is-success div');
        const actualText = notification.getText()
        assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    }

    redirectedTo(route){
        const url = new URL(browser.getUrl());
        const actualUrl = url.host.toString() + url.pathname.toString();
        assert(actualUrl.includes(route), `Expected ${url} to contain ${route}`);
    }
}

module.exports = new AssertHelper();